# MineSweeper
This project is built using react and redux

## Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

#### Tested in macOs Sierra V10.12.6, Node V: v7.10.0, NPM V: 4.2.0
  
<img src="https://gitlab.com/rajzone/MineSweeper4VivLabs/raw/master/GameProgress.png" />


<img src="https://gitlab.com/rajzone/MineSweeper4VivLabs/raw/master/GameOver.png" />

